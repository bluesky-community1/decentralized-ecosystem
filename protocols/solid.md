# Solid

Solid (derived from "social linked data") is a protocol to separate applications from data and to give users control of their data. This adds several things to existing Web protocols: standardized identity integration, access control systems, and read-write interfaces over HTTP. 

While Web platforms today have their own protocols and APIs for storing and accessing data for getting control, the Solid universal model means any application can write any data of its choosing anywhere on the Web. Solid allows applications to read and write to a private server, contribute to others’ Pods (derived from "personal online data stores"), and make it easy to build new relationships.

### Identity

"[Any resource of significance should be given a URI](https://www.w3.org/DesignIssues/Axioms#Universality2)".

By applying the best practices of the [Architecture of the World Wide Web](https://www.w3.org/TR/webarch/), Solid uses *WebID* as a universal identification mechanism. A WebID is an HTTP URI which refers to an Agent (Person, Organization, Group, Device, etc.). A description of the WebID can be found in the Profile Document, a type of web page that any social network user is familiar with. Individuals can associate themselves with multiple identities, and they can be public or private. Identities can be interlinked with one another.

Tim Berners-Lee's WebID: [https://www.w3.org/People/Berners-Lee/card#i](https://www.w3.org/People/Berners-Lee/card#i)

WebID is integral to authenticating identities and used in setting fine grained access controls to Web resources. Different authentication mechanisms can be loosely coupled with WebID (orthogonal specifications). The [Solid OpenID Connect](https://solid.github.io/authentication-panel/solid-oidc/) (Solid OIDC) specification defines how resource servers verify the identity of relying parties and end users based on the authentication performed by an OpenID provider. The WebID-TLS protocol specifies how a service can authenticate a user after requesting their Certificate without needing to rely on this being signed by a well known Certificate Authority.

### Network

The [Solid Protocol](https://solidproject.org/TR/protocol) specifies how clients and servers exchange data securely over the Internet, and they do so using the HTTP Web standard. The WebSocket Protocol is used for live updates between clients and servers.

Clients request read-write operations using various HTTP methods (GET, OPTIONS, POST, PUT, PATCH, DELETE..) on resources identified by URLs. The [Linked Data](https://www.w3.org/DesignIssues/LinkedData) design principles are applied to identify, describe, and discover human- and machine-readable information. Resources may be publicly accessible, require credential or capability based authorization. 

### Data

Pods are data stores that serve as a standardized place for individuals to manage any kind of data using disparate applications, and apply identity-based control permissions for data sharing.

Solid users may self-host their Pods (on their own server) or use a Pod provider. Applications read and write data into the Pod depending on the authorizations granted by the users associated with that Pod. A person may have multiple Pods. Data may be replicated across Pods.

Because Solid Pods are generic universal data stores, the application level data standards need to be understood only by the applications. These “client-client” specs exist for basic profile and contact information as well as chat, and will be developed for other areas such as calendars, fitness, financial, etc. The goal is that all personal data will be in Pods. Users are free to switch between applications and share data between them.

*Resource Description Framework* (RDF) provides an interoperable means of publishing and linking self-describing Web resources where assertions can be made about anything at any level of abstraction in the form of triple statements.

Solid supports sharing and reuse of notifications across applications, regardless of how they were generated.


### Moderation & Reputation

Solid is a decentralized system of information storage and sharing, where moderation could be done by individuals or groups with access privileges to the resources in a Pod (e.g. inbox management, filtering annotations).  

The Solid platform allows you to build all kinds of collaborative systems, such as issue flagging, escalation and management, so any community can make or tweak its own apps for things like collaborative moderation, and engineering is more tailored to community and culture. 

### Social & Discovery

Solid (**social** linked data) enables social interactions by using decentralised identifiers, data, and applications. Any Web resource can advertise a location to receive notifications about (social) activities e.g. invitation to review an article, a public comment or annotation created somewhere on the Web, a bookmark,like activity, or a follow. A large multi-user Solid social chat application for example could read/write data for each participant in their own Pod, owned by them, with access and duration controlled by them. 

Solid uses the W3C [Linked Data Notifications](https://www.w3.org/TR/ldn/) (LDN) recommendation as a communication protocol that describes how servers (receivers) can have messages pushed to them by applications (senders), as well as how other applications (consumers) may retrieve those messages. Any resource can advertise a receiving endpoint (Inbox) for the messages. Inboxes can be private or shared with individuals or groups, and can be access controlled.

Solid servers can offer federated [SPARQL](https://www.w3.org/TR/sparql11-overview/) queries as well as [Linked Data Fragments](https://linkeddatafragments.org/) with different interfaces to redistribute the load between clients and servers.

### Privacy & Access Control

Solid was created on the principle of enhancing user control of their data. Privacy and access controls are core to the mission of safer data sharing through standardization and simplification. Solid offers the ability to run as a decentralised privacy system, such that no central authority is required for massive scale storage and communications. Access control of resources is for trusted peers, where communication between parties can be undetectable and unobservable by unauthorised third-parties.

Attribute Based Access Control (ABAC) is provided through Web Access Control (WAC), a decentralized cross-domain system. The WAC mechanism is concerned with giving access to agents denoted by an identity (e.g. WebID) to perform various kinds of read-write operations on resources identified by URLs. 

The Access Control List (ACL) ontology is used to describe authorization policies about authorized agents with modes of access on target resources, similar to Role Based Access Control (RBAC). Further feature models developing privately are under consideration, such as Access Control Policies (ACP), as Solid’s data access framework can be extended by the community.

Applications can determine access privileges that are available to the user and public for any given resource.

Policies can be attached to assets to permit or prohibit actions, be limited by constraints or duties, as well as have obligations required to be met by stakeholders. Data privacy can be described and represented by vocabularies to help conform to regulations such as handling personal data (e.g. EU General Data Protection Regulation).

Data can be encrypted in transit between an application and a Pod, as well as stored encrypted. Pod providers and data owners also can encrypt data such that Pod owner data is not exposed to the service.


### Interoperability

Solid servers can be built atop of W3C [Linked Data Platform](https://www.w3.org/TR/ldp/) and [Web Annotation Protocol](https://www.w3.org/TR/annotation-protocol/) servers.

LDN notifications are in RDF (JSON-LD and other RDF syntaxes can be content negotiated). To allow a wide variety of use cases, notifications can contain any data and be described with any vocabulary e.g. W3C [Activity Streams](https://www.w3.org/TR/activitystreams-vocabulary/).

W3C [ActivityPub](https://www.w3.org/TR/activitypub/) uses LDN's targeting and delivery mechanism, which is required by its server-to-server protocol for a federated social system. A Solid server can potentially act as an ActivityPub server.


### Scalability

Scale on the web with the decentralized nature of Solid means an application needs to direct requests to an appropriate server and enable that server to process for an appropriate response. There is a rich history of web application scale optimisation techniques to draw from (caching, store-and-forward, aggregation and a mix of these).

The network topology of Solid by default is point-to-point (e.g. connections between two endpoints). The authority topology is that nodes interact directly with each other, without scale being held back by third-party nodes. There is no single point of failure, high availability, system performance, and is generally survivable. Solid does not shard or replicate by default, with data localized to a Pod and storage controlled by the owner.


### Governance & Business Models

The W3C mission is to lead the World Wide Web to its full potential by developing protocols and guidelines that ensure the long-term growth of the Web.

The W3C shares similar views to the IETF, ISOC, ICANN, and others in the Internet/Web technical standards ecosystem working on governance of communication platforms so important to humanity. 

W3C governance for the Solid standard is aligned with W3C ethical Web principles such as neutrality, multi-stakeholder and balanced participation, open and due process, consensus based decisions, open deliverables, and online accountability/transparency.

Solid business models are a reflection of the human condition, constrained only by imagination and regulation. A Solid server that holds data in Pods, for example, could be a business in running a Solid service itself or related to business in handling the data served via Solid (e.g. from information assets such as original works to information about assets such as shopping carts). 

Much in the same way a Web server created almost infinite business models, Solid in fact expands the Web by decoupling application development and identity management from data storage (Solid Pods) -- more apps can be written with more identity management options to access much more data maintained more carefully for more sharing.

### Implementations & Applications

Social, Healthcare, Fitness, Finance, Retail, AI, Scientific communication..

The Solid community develops and supports a wide range of [applications](https://solidproject.org/apps). Applications generate notifications about activities, interactions, and new information, which may be presented to the user or processed further.

The eventual vision of Solid is that all data of all applications is stored in pods somewhere, and all applications can access that data using interoperable standards. There is significant progress, significant momentum, and a huge amount to be done.

Example Solid applications:

* [SolidOS](https://solidos.solidcommunity.net/) is a Web-based Operating System for any new computer or data store.
* [dokieli](https://dokie.li/) is a client-side editor for decentralised article publishing, annotations and social interactions.
* [Media Kraken](https://noeldemartin.github.io/media-kraken/) is an application to track films and create collections.

Open Source Solid servers:

* [Node Solid Server](https://github.com/solid/node-solid-server/)
* [Community Solid Server](https://github.com/solid/community-server/)
* [PHP Solid Server](https://github.com/pdsinterop/php-solid-server/)

Commercial Solid servers:
* [Enterprise Solid Server](https://docs.inrupt.com/ess/)


### Links

* [Solid Project](https://solidproject.org/)
* [Solid Technical Reports](https://solidproject.org/TR/)
* [W3C Solid Community Group](https://www.w3.org/community/solid/)
* [Solid repositories](https://github.com/solid/)
* [Solid Roadmap](https://solidos.solidcommunity.net/public/Roadmap/Tasks/)
